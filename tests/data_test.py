import unittest
from data import *
from for_test_xml import *


class ParseTest(unittest.TestCase):
    def test_data(self):
        self.assertEqual(parse_xml(response=task_tests), ["AUD", 16.0102])


if __name__ == '__main__':
    unittest.main()
