FROM python

WORKDIR /app/parse-xml

RUN pip install requests
RUN pip install --upgrade pip

COPY docker /parse-xml

CMD ["python", "data.py"]