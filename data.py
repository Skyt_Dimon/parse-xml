import requests
import xml.dom.minidom
import json

headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
}
url = 'http://www.cbr.ru/scripts/XML_daily.asp'


def parse_xml(response):
    dom = xml.dom.minidom.parseString(response.text)
    element = dom.getElementsByTagName('Valute')
    array = {}

    for node in element:
        for child in node.childNodes:
            if child.nodeType == 1:
                if child.tagName == 'Value':
                    if child.firstChild.nodeType == 3:
                        value = float(child.firstChild.data.replace(",", "."))
                elif child.tagName == "CharCode":
                    if child.firstChild.nodeType == 3:
                        char_code = child.firstChild.data
        array[char_code] = value
    for i in array:
        x = [json.dumps([i, array[i]])]
        print(*x)


def main():
    ses = requests.Session()
    response = ses.get(url=url, headers=headers)
    parse_xml(response=response)
    with open("tests/xml.xml", "w") as file:
        file.write(response.text)


if __name__ == "__main__":
    main()
